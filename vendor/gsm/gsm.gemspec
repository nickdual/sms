$:.push File.expand_path("../lib", __FILE__)
require "version"
Gem::Specification.new do |s|
  s.name        = 'gsm'
  s.version     = GSM::VERSION
  s.date        = Time.now.strftime("%Y-%m-%d")
  s.platform    = Gem::Platform::RUBY
  s.summary     = "GSM"
  s.description = "GSM"
  s.authors     = ["BPT2410"]
  s.email       = 'phutuong24101990@gmail.com'
  s.require_paths = ["lib"]
  s.files = %w(LICENSE README Rakefile) + Dir.glob("{lib,spec}/**/*")
  s.homepage    =
    'http://rubygems.org/gems/gsm'
  s.add_dependency "serialport"
end
