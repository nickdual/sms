# GSM gem : use for connect to GSM modem, send sms from GSM modem

require 'serialport'
require 'time'
require 'date'
require 'fileutils'
load "exception.rb"
load "sms.rb"
load "version.rb"
module GSM
  class Modem
    attr_accessor :smsc

    def initialize(options = {})
      begin
        @port = SerialPort.new(options[:port] || 0, options[:baud] || 38400, options[:bits] || 8, options[:stop] || 1, SerialPort::NONE)
        @debug = options[:debug]
        @is_connect = true
        @is_save = options[:save]
        result = cmd("AT")
        @is_connect = (result.index("OK").nil?) ? false : true
        cmd("AT+CMGF=1")
        @smsc = cmd("AT+CIMI")
        @smsc = @smsc.strip[0..14]
      rescue Exception => ex
        @is_connect = false
        raise "Error : #{ex.message}"
      end

    end

    # Get manufacturer of GSM Modem
    def manufacturer
      result = cmd("AT+CGMI")
      str = result.split("\r\n")
      (str[3].present? && str[3] == "OK") ? str[1] : "Unknown"
    end

    # Get frequency of GSM Modem
    def frequency
      result = cmd("AT+CGMM")
      str = result.split("\r\n")
      (str[3].present? && str[3] == "OK") ? str[1] : "Unknown"
    end

    # Get software of GSM Modem
    def software
      result = cmd("AT+CGMR")
      str = result.split("\r\n")
      (str[3].present? && str[3] == "OK") ? str[1] : "Unknown"
    end

    # Get IMEI number of GSM Modem
    def IMEI
      result = cmd("AT+CGSN")
      str = result.split("\r\n")
      (str[3].present? && str[3] == "OK") ? str[1] : "Unknown"
    end

    # Get phone number of Sim card
    def phone_number
      result = cmd("AT+CUSD=1,\"*888#\",\"15\"")
      sleep 3
      result = wait
      if result.nil?
        return "Unknown"
      end
      result = result[12..result.length - 7].scan(/.{1,4}/m)
      result_money = ""
      result.each do |value|
        result_money += [value.to_i(16)].pack("U*")
      end
      result_money.encode("utf-8").force_encoding("utf-8")
    end

    # Get balance of Sim card
    def money(type)
      result = cmd("AT+CUSD=1,\"#{type}\",\"15\"")
      sleep 3
      result = wait
      result = result[12..result.length - 7].scan(/.{1,4}/m)
      result_money = ""
      result.each do |value|
         result_money += [value.to_i(16)].pack("U*")
      end
      result_money.encode("utf-8").force_encoding("utf-8")
    end

    # Add credit for Sim card::
    # @param card_id : ID of card
    def add_credit(card_id)
      result = cmd("AT+CUSD=1,\"*100*#{card_id}#\",\"15\"")
      sleep 3
      result = wait
      result = result[12..result.length - 7].scan(/.{1,4}/m)
      result_money = ""
      result.each do |value|
        result_money += [value.to_i(16)].pack("U*")
      end
      result_money.encode("utf-8").force_encoding("utf-8")
    end

    def card_id
      result = cmd("AT+CCID")
      str = result.split("\r\n")
      (str[3].present? && str[3] == "OK") ? str[1] : "Unknown"
    end

    # Check connect GSM Modem
    def connect?
      @is_connect
    end

    # Disconnect GSM Modem
    def close
      if @is_connect
        @port.close
      end
    end

    # Send SMS
    def send_sms(options)
      cmd("AT+CMGS=\"#{options[:number]}\"")
      cmd("#{options[:message][0..140]}#{26.chr}\r\r")
      sleep 3
      result = wait
      cmd("AT")
      cmd("AT+CMGW=\"#{options[:number]}\",145,\"STO SENT\"")
      cmd("#{options[:message][0..140]}#{26.chr}\r\r")
      (result.index("OK").nil?) ? false : true
    end

    # Get messages of inbox
    def inbox
      sms = cmd("AT+CMGL=\"ALL\"")
      # Ugly, ugly, ugly!
      msgs = sms.scan(/\+CMGL\:\s*?(\d+)\,.*?\,\"(.+?)\"\,.*?\,\"(.+?)\".*?\n(.*)/)
      return nil unless msgs
      msgs.collect! { |m| GSM::SMS.new(:connection => self, :id => m[0], :sender => m[1], :time => m[2], :message => m[3].chomp) } rescue nil
    end

    # Get messages of outbox
    def outbox
      sms = cmd("AT+CMGL=\"STO SENT\"")
      # Ugly, ugly, ugly!
      msgs = sms.scan(/\+CMGL\:\s*?(\d+)\,.*?\,\"(.+?)\"\,.*?\,(.+?).*?\n(.*)/)
      return nil unless msgs
      msgs.collect! { |m| GSM::SMS.new(:connection => self, :id => m[0], :sender => m[1], :time => m[2], :message => m[3].chomp) } rescue nil
    end

    # Delete message id::
    # @param id : ID of message
    def delete(id)
      result = cmd("AT+CMGD=#{id}")
      (result.index("OK").nil?) ? false : true
    end

    # Get message information::
    # @param id : ID of message
    def get(id)
      sms = cmd("AT+CMGR=#{id}")
      msgs = sms.scan(/\+CMGR\:\s\"(.+?)\"\,\"(.+?)\"\,.*?\,\"(.+?)\".*?\n(.*)/)
      puts msgs.to_yaml
      return nil unless msgs
      messages = msgs.collect! { |m| GSM::SMS.new(:connection => self, :id => id, :sender => m[1], :time => m[2], :message => m[3].chomp) } rescue nil
      messages.first
    end


    private
      def cmd(cmd)
        @port.write(cmd + "\r")
        wait
      end

      def wait
        buffer = ''
        while IO.select([@port], [], [], 0.25)
          chr = @port.getc.chr;
          print chr if @debug == true
          buffer += chr
        end
        buffer
      end
  end

end