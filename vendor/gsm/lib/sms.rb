# To change this template, choose Tools | Templates
# and open the template in the editor.
module GSM
  class SMS
    attr_accessor :id, :sender, :message, :connection
    attr_writer :time

    def initialize(params)
      @id = params[:id]; @sender = params[:sender]; @time = params[:time]; @message = params[:message]; @connection = params[:connection]
    end

    def delete
      result = @connection.cmd("AT+CMGD=#{@id}")
      (result.index("OK").nil?) ? false : true
    end

    def time
      # This MAY need to be changed for non-UK situations, I'm not sure
      # how standardized SMS timestamps are..
      @time.present? ? Time.strptime(@time, "%y/%m/%d,%H:%M:%S") : @time
    end
    
  end
end
