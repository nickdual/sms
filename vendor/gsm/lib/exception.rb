module GSM
  class Exception
    attr_accessor :code, :message

    # initialize Response
    def initialize(code, message, options = {})
      @code, @message = code, message
      @options = options
    end

    # return json format
    def to_json
      {"code" => @code, "message" => @message, "results" => @options}
    end

  end
 
end