class CreateMessages < ActiveRecord::Migration
  def change
    create_table :messages do |t|
      t.string :from, :default => "0123456789"
      t.string :to, :default => "0123456789"
      t.text :content
      t.string :type, :default => "Inbox"
      t.timestamps
    end
  end
end
