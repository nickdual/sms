class Message
  include Mongoid::Document
  field :from, :type => String
  field :to, :type => String
  field :content, :type => String
  field :message_type, :type => String
  
  def send_sms
    p = GSM::Modem.new({:port => "/dev/ttyUSB1", :debug => true})
    # Send a text message
    result = p.send_sms(:number => self.to, :message => self.content) if p.connect?
    p.close
    result
  end
  
  def self.inbox
    p = GSM::Modem.new({:port => "/dev/ttyUSB1", :debug => true})
    messages = (p.connect?) ? p.inbox : nil
    p.close
    messages
  end

  def self.outbox
    p = GSM::Modem.new({:port => "/dev/ttyUSB1", :debug => true})
    messages = (p.connect?) ? p.outbox : nil
    p.close
    messages
  end

  def self.smsc
    p = GSM::Modem.new({:port => "/dev/ttyUSB1", :debug => true})
    smsc = (p.connect?) ? p.smsc : nil
    p.close
    smsc
  end

  def self.delete(id)
    p = GSM::Modem.new({:port => "/dev/ttyUSB1", :debug => true})
    result = (p.connect?) ? p.delete(id) : nil
    p.close
    result
  end

  def self.get_by_id(id)
    p = GSM::Modem.new({:port => "/dev/ttyUSB1", :debug => true})
    result = (p.connect?) ? p.get(id) : nil
    p.close
    result
  end
  
end
