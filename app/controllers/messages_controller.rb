class MessagesController < ApplicationController
  def index
    @messages = Message.inbox
  end
  
  def new
    @message = Message.new({:from => Message.smsc})
  end
  
  def show
    @new_message = Message.new({:from => Message.smsc})
    @message = Message.get_by_id(params[:id])
  end
  
  def destroy
    if Message.delete(params[:id])
      flash[:notice]='Message was successfully deleted.'
      redirect_to '/messages' and return
    else
      flash[:error]='Message was fail deleted.'
      redirect_to '/messages' and return
    end

  end
  
  def create
    @message = Message.new(params[:message])
    @message.message_type = "Outbox"
    respond_to do |format|
      if @message.save && @message.send_sms
        flash[:notice]='Message was successfully created.'
        format.html { redirect_to "/messages"}
        format.json { head :no_content }
      else
        format.html { render action: "new" }
        format.json { render json: @message.errors, status: :unprocessable_entity }
      end
    end
  end
  
  def outbox
    @messages = Message.outbox
  end
  
  def draft
    
  end

  def balance
    @gsm_modem = GSM::Modem.new({:port => "/dev/ttyUSB1", :debug => true})
    @balance = @gsm_modem.money("*101#")
    @bonus_balance = @gsm_modem.money("*102#")
    @gsm_modem.close
  end

  def add_credit
    @gsm_modem = GSM::Modem.new({:port => "/dev/ttyUSB1", :debug => true})
    @messages = @gsm_modem.add_credit(params[:card_id])
  end

end
